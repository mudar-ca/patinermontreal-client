String? asStringOrNull(dynamic item) => item is String ? item : null;

bool? asBoolOrNull(dynamic item) => item is bool ? item : null;

DateTime? asDateTimeOrNull(dynamic item) {
  switch (item.runtimeType) {
    case DateTime:
      return item;
    case String:
      return DateTime.tryParse(item);
    default:
      return null;
  }
}
