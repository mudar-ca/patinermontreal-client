import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';

final logger = Logger(
  filter: ProductionFilter(),
  level: kDebugMode.logLevel(),
);

extension on bool {
  Level logLevel() {
    if (this) {
      return Level.verbose;
    } else {
      return Level.info;
    }
  }
}
