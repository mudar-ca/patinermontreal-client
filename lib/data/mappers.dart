import 'package:geojson/geojson.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../models/park.dart';
import '../utils/logger.dart';

extension FeatureMapper on GeoJsonFeature {
  Park? toPark() {
    try {
      if (this is GeoJsonFeature<GeoJsonPoint>) {
        final properties = this.properties;
        if (properties == null) {
          throw ArgumentError('properties cannot be null');
        }
        final geoPoint = geometry?.geoPoint;
        if (geoPoint == null) {
          throw ArgumentError('geometry Point cannot be null');
        }

        return Park.fromJson(
          properties,
          geoPoint.latitude,
          geoPoint.longitude,
        );
      } else {
        throw ArgumentError('geometry type must be Point');
      }
    } on ArgumentError catch (e) {
      logger.e('Rink fromJson() parsing failed: $e');
      return null;
    }
  }
}

extension MarkerBuilder on Park {
  Marker toMarker({Function()? onTapCallback}) => Marker(
        markerId: MarkerId(id),
        position: LatLng(latitude, longitude),
        onTap: onTapCallback,
      );
}
