import 'dart:convert';
import 'dart:io';

import 'package:geojson/geojson.dart';
import 'package:http/http.dart' as http;

Future<GeoJsonFeatureCollection> fetchParks() async {
  const url = 'https://patinermontreal-api.herokuapp.com/';

  // Retrieve the location of rinks
  final response = await http.get(Uri.parse(url));

  if (response.statusCode == 200) {
    return featuresFromGeoJson(Utf8Decoder().convert(response.bodyBytes));
  } else {
    throw HttpException(
        'Unexpected status code ${response.statusCode}:'
        ' ${response.reasonPhrase}',
        uri: Uri.parse(url));
  }
}
