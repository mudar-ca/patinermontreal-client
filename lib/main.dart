import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import './data/locations.dart' as locations;
import './data/mappers.dart';
import './models/park.dart';
import './utils/logger.dart';
import 'widgets/park_details_sheet.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final Set<Marker> _markers = {};
  final ValueNotifier<Park?> _park = ValueNotifier(null);

  static final CameraPosition montrealCameraPosition = CameraPosition(
    bearing: -34,
    target: const LatLng(45.508830, -73.554112),
    zoom: 10,
  );

  Future<void> _onMapCreated(BuildContext context) async {
    _addMarkersToMap(context);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: const Text('Patiner Montréal'),
            backgroundColor: Colors.blue,
          ),
          body: Stack(
            children: [
              GoogleMap(
                onMapCreated: (argument) => _onMapCreated(context),
                initialCameraPosition: montrealCameraPosition,
                markers: _markers,
              ),
              ParkDetailSheet(_park),
            ],
          )),
    );
  }

  void _addMarkersToMap(BuildContext context) async {
    final parks = await locations.fetchParks();
    logger.i('Parks found: ${parks.collection.length}');

    setState(() {
      _markers.clear();
      for (final feature in parks.collection) {
        final park = feature.toPark();
        if (park != null) {
          final marker = park.toMarker(onTapCallback: () {
            logger.i('onTap: park: ${park.name}');
            _park.value = park;
          });
          _markers.add(marker);
        }
      }
    });
  }
}
