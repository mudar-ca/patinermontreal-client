import 'package:collection/collection.dart';

import './rink.dart';
import '../utils/logger.dart';
import '../utils/type_casting.dart' as type_casting;

class Park {
  Park(
    this.id,
    this.slug,
    this.name,
    this.municipality,
    this.address,
    this.rinks,
    this.updatedAt,
    this.latitude,
    this.longitude,
  ) {
    if (id.isEmpty) throw ArgumentError('id cannot be empty');
    if (slug.isEmpty) throw ArgumentError('slug cannot be empty');
    if (name.isEmpty) throw ArgumentError('name cannot be empty');
  }

  final String id;
  final String slug;
  final String name;
  final String municipality;
  final String address;
  final List<Rink> rinks;
  final DateTime? updatedAt;
  final double latitude;
  final double longitude;

  static Park? fromJson(
    Map<String, dynamic> data,
    double latitude,
    double longitude,
  ) {
    List<Rink>? rinks;
    try {
      rinks = (data['rinks'] as List<dynamic>?)
          ?.map((e) => Rink.fromJson(e))
          .whereNotNull()
          .toList();
    } on ArgumentError catch (e) {
      logger.e('Park rinkList fromJson() parsing error: $e');
    } on TypeError catch (e) {
      logger.e('Park rinkList fromJson() parsing error: $e');
    }

    try {
      return Park(
        data['id'] ?? data['slug'],
        data['slug'] ?? '',
        data['name'] ?? '',
        type_casting.asStringOrNull(data['municipality']) ?? '',
        type_casting.asStringOrNull(data['address']) ?? '',
        rinks ?? List.empty(),
        type_casting.asDateTimeOrNull(data['updatedAt']),
        latitude,
        longitude,
      );
    } on ArgumentError catch (e) {
      logger.e('Park fromJson() parsing failed: $e');
      return null;
    } on TypeError catch (e) {
      logger.e('Park fromJson() parsing failed: $e');
      return null;
    }
  }
}
