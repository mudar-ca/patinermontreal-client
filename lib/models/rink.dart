import '../utils/logger.dart';
import '../utils/type_casting.dart' as type_casting;

class Rink {
  Rink(
    this.description,
    this.isHockey, {
    this.isRefrigerated = false,
    this.isOpen,
    this.condition,
  }) {
    if (description.isEmpty) throw ArgumentError('description cannot be empty');
  }

  final String description;
  final bool isHockey;
  final bool isRefrigerated;
  final bool? isOpen;
  final String? condition;

  static Rink? fromJson(Map<String, dynamic> data) {
    try {
      return Rink(
        data['description'],
        data['isHockey'],
        isRefrigerated:
            type_casting.asBoolOrNull(data['isRefrigerated']) ?? false,
        isOpen: type_casting.asBoolOrNull(data['isOpen']),
        condition: type_casting.asStringOrNull(data['condition']),
      );
    } on ArgumentError catch (e) {
      logger.e('Rink fromJson() parsing failed: $e');
      return null;
    } on TypeError catch (e) {
      logger.e('Rink fromJson() parsing failed: $e');
      return null;
    }
  }
}
