import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../models/park.dart';

class ParkDetailSheet extends StatefulWidget {
  ParkDetailSheet(this.park);

  final ValueListenable<Park?> park;

  @override
  State<StatefulWidget> createState() => _ParkDetailSheetState();
}

class _ParkDetailSheetState extends State<ParkDetailSheet> {
  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
        initialChildSize: .2,
        minChildSize: .1,
        maxChildSize: .6,
        builder: (BuildContext context, ScrollController scrollController) {
          return Container(
            color: Colors.white,
            child: ValueListenableBuilder<Park?>(
              valueListenable: widget.park,
              builder: (BuildContext context, Park? value, Widget? child) {
                return ListView(
                  controller: scrollController,
                  children: [
                    ListTile(
                      title: Text('Park'),
                      subtitle: Text(value?.name ?? 'UNKNOWN'),
                    ),
                    ListTile(
                      title: Text('Address'),
                      subtitle: Text(value?.address ?? 'UNKNOWN'),
                    ),
                    ListTile(
                      title: Text('Municipality'),
                      subtitle: Text(value?.municipality ?? 'UNKNOWN'),
                    ),
                    ListTile(
                      title: Text('Updated at'),
                      subtitle: Text(
                          value?.updatedAt?.toIso8601String() ?? 'UNKNOWN'),
                    ),
                  ],
                );
              },
            ),
          );
        });
  }
}
